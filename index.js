#!/usr/bin/env node

const chalk = require('chalk');
const figlet = require('figlet');
const { Plop, run } = require('plop');

console.log(' ');
console.log(chalk.bold.green(figlet.textSync('\n\n mrcodecli ', {
  font: 'ANSI Shadow',
  horizontalLayout: 'default',
  verticalLayout: 'default',
  whitespaceBreak: false
})));
console.log(`${chalk.bold.bgYellow.black('Author:')} ${chalk.white(' MrCodeDev')}`);
console.log(`${chalk.bold.bgYellow.black('Version:')} ${chalk.white('1.0.0.')}`);
console.log('----------------------------------------------------------------------------');
console.log(' ');

Plop.launch({
  configPath: './generators/plopfile.js',
}, run);