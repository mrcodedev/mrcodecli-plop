const requiredFieldApp = (fieldName) => (value) => {
    if(String(value).length === 0) {
      return `Name of App, is required`
    }
    return true
}

const requiredFieldComponent = (fieldName) => (value) => {
  if(String(value).length === 0) {
    return `Name of Component, is required`
  }

  if(!String(value).includes('-')) {
    return `The name "${value}", should have at least one dash. Example: my-component`
  }

  return true
}

module.exports = function (plop) {
  const currentPath = process.env.INIT_CWD;

  plop.setGenerator("lit:app", {
    description: 'Creating App scaffolding for LitElement',
    prompts: 
      [
        {
          type: 'input',
          name: 'nameApp',
          message: 'What name do you want to give to App? 📝',
          validate: requiredFieldApp('nameApp')
        },
        {
          type: 'input',
          name: 'descriptionApp',
          message: 'Write a description of your App ✎',
        }
      ],
    actions: 
      [
        {
          type: 'add',
          path: process.cwd() + '/app-{{kebabCase nameApp}}/src/components/remove',
          templateFile: 'templates/app/src/components/components.txt'
        },
        {
          type: 'add',
          path: process.cwd() + '/app-{{kebabCase nameApp}}/src/app-component.js',
          templateFile: 'templates/app/src/app-component.hbs'
        },
        {
          type: 'add',
          path: process.cwd() + '/app-{{kebabCase nameApp}}/src/app-component.scss',
          templateFile: 'templates/app/src/app-component.scss'
        },
        {
          type: 'add',
          path: process.cwd() + '/app-{{kebabCase nameApp}}/.editorconfig',
          templateFile: 'templates/app/.editorconfig'
        },
        {
          type: 'add',
          path: process.cwd() + '/app-{{kebabCase nameApp}}/.gitignore',
          templateFile: 'templates/app/.gitignore'
        },
        {
          type: 'add',
          path: process.cwd() + '/app-{{kebabCase nameApp}}/index.html',
          templateFile: 'templates/app/index.hbs'
        },
        {
          type: 'add',
          path: process.cwd() + '/app-{{kebabCase nameApp}}/package.json',
          templateFile: 'templates/app/package.json'
        }
      ]
    }
  );
  plop.setGenerator("lit:component", {
    description: 'Creating Component scaffolding for LitElement',
    prompts: [
      {
        type: 'input',
        name: 'nameComponent',
        message: 'What name do you want to give to Component? (text in kebabCase) 📝',
        validate: requiredFieldComponent('nameComponent')
      },
      {
        type: 'input',
        name: 'descriptionComponent',
        message: 'Write a description of your element ✎',
      }
    ],
    actions: 
    [
      {
        type: 'add',
        path: process.cwd() + '/{{kebabCase nameComponent}}/index.js',
        templateFile: 'templates/component/index.js.hbs'
      },
      {
        type: 'add',
        path: process.cwd() + '/{{kebabCase nameComponent}}/{{kebabCase nameComponent}}.js',
        templateFile: 'templates/component/component-name.hbs'
      },
      {
        type: 'add',
        path: process.cwd() + '/{{kebabCase nameComponent}}/README.md',
        templateFile: 'templates/component/README.hbs'
      },
      {
        type: 'add',
        path: process.cwd() + '/{{kebabCase nameComponent}}/test/{{kebabCase nameComponent}}.test.js',
        templateFile: 'templates/component/test/component.test.js.hbs'
      },
      {
        type: 'add',
        path: process.cwd() + '/{{kebabCase nameComponent}}/src/{{pascalCase nameComponent}}.js',
        templateFile: 'templates/component/src/ComponentName.js.hbs'
      },
      {
        type: 'add',
        path: process.cwd() + '/{{kebabCase nameComponent}}/src/{{pascalCase nameComponent}}.scss',
        templateFile: 'templates/component/src/ComponentStyle.scss'
      }
    ]
  });
};